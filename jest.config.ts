import type { Config } from "@jest/types"
const blitzPreset = require("blitz/jest-preset")

const config: Config.InitialOptions = {
  ...blitzPreset,
  projects: blitzPreset.projects.map((p) => ({
    ...p,
    testPathIgnorePatterns: [...p.testPathIgnorePatterns, "<rootDir>/cache/"],
  })),
}

export default config
